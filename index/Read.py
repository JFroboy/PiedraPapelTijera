from collections import Counter
from random import choice

class Read:
    """
    tablero: Lista de listas que representa el tablero de juego
    tamanio: El tamaño de la linea (fila)
    arbol: Lista de los nodos que se van creando
    prioridad: lista de tuplas de 2 elementos (posicion_arbol, f)
    camino_optimo: Lista que almacena los 3 robots (piedra,papel,tijera) de los nodos que son el camino mas optimo
    h: Son las casillas que le faltan robot o robots que estan mas cerca a la meta de llegar a ella
    cada nodo tendra las siguientes claves:
        padre: posicion en la lista arbol del nodo padre
        posicion_arbol: posicion del nodo en el arbol
        posicion_tablero: posicion actual del nodo en el tablero
        piedra,papel,tijera: lista de 4 elemnetos con los atributos de cada robot
            1. Boolean: True Si el robot esta libre o False si se encuentra luchando contra algun obstaculo.
            2. Int: Las unidades de tiempo que le tomara vencer a un grupo de obstaculos.
            3. Int: Posicion en el tablero en la que se encuentra el robot.
            4. Boolean: True si el robot fue el primero en ser enviado a luchar contra los obstaculos.
                En el momento en que un robot llega a una casilla en la cual los otros dos estan combatiendo. la
                variable permite identificar a cual de los dos hay que prestarle la 'ayuda'
    """
    tablero = []
    tamanio = 0
    nodo = {"padre": "null", "posicion_arbol": 0, "posicion_tablero": -1, "g": 0, "h": 100, "f": 0,
            "piedra": [True, -1, -1, False],
            "papel": [True, -1, -1, False],
            "tijera": [True, -1, -1, False]}
    arbol = [nodo]
    prioridad = [[0, 0, 100]]
    camino_optimo = []

    def read_file(self, file_name):
        """
        Toma cada linea y es leida por un para almacenar cada elemento en una lista.
        :param file_name: ruta de la ubicacion del archivo
        :return: void
        """
        self.tablero = []
        archivo = open(file_name, "r")

        for line in archivo.readlines():
            self.tamanio = len(line)
            values_line = []
            for celda in line:
                values_line.append(celda)
            """ Como al final de cada linea hay un salto, se elimina el ultimo elemento"""
            values_line.pop()
            self.tablero.append(values_line)

    def len_read_file(self, file_name):
        """
        Valida que el tablero del documento tenga exactamente tres caminos o filas
        :param file_name: ruta de la ubicacion del archivo
        :return: boolean
        """
        file_len = len(open(file_name).readlines())
        if file_len < 3 or file_len > 3:
            return False
        return True

    def validations_tablero(self):
        """
        Valida que el tablero cumpla con las restricciones.
            -Las filas tengan igual tamaño
            -Que exista por lo menos un espacio en blanco entre dos obstaculos
             (Utiliza la funcion aux -> obstaculos_intermedios)
            -Que una columna tenga exactamente 3 obstaculos
        :return: Boolean
        """
        tamanio_acumulado = -1
        for value in self.tablero:
            if tamanio_acumulado < 0:
                """
                Si es la primer linea en ser leida, se actualiza el valor de tamanio_acumulado, luego se comprueba que 
                se cumpla con el espacio entre obstaculos
                """
                tamanio_acumulado = len(value)
                if not self.obstaculos_intermedios(value):
                    return False

            elif tamanio_acumulado == len(value) and len(value) in range(1, 51):
                """
                Se comprueba que el tamaño de la linea actual sea igual a la anterior y tambien que se encuentre en
                un rango entre 1 y 50
                """
                if not self.obstaculos_intermedios(value):
                    return False
            else:
                return False
        if not self.obstaculos_columna():
            """Para finalizar se verifica que se cumpla la ultima restriccion"""
            return False
        return True

    def obstaculos_intermedios(self, fila):
        """
        Verifica que exista un espacio entre dos obstaculos y que no exista mas de tres robots sobre el tablero.
        Si la variable libre == True significa en que en la casilla puede ir cualquier elemento obstaculo o espacio en
            blanco.
        Si en la fila actual hay un robot se comprueba que la variable libre == True si es así, su valor cambia a False
            de lo contrario se retorna un False.
        Por otro lado si hay un espacio en blanco el valor de la variable libre se cambia a True
        En el momento que encuentre un robot se retorna False
        :param fila: fila del tablero a evaluar
        :return: Boolean
        """
        obstaculos = ["L", "H", "V"]
        robots = ["R", "P", "T"]
        libre = True

        for n, value in enumerate(fila):
            if n == len(fila) - 1:
                if not (value == "0"):
                    return False
            elif len(fila) < 3:
                if not (fila[n] == "0" and fila[n+1] == "0"):
                    return False
            else:
                if libre:
                    if value in obstaculos:
                        libre = False
                    elif value in robots:
                        return False

                elif value == "0":
                    libre = True
                else:
                    return False
        return True

    def obstaculos_columna(self):
        """
        Comprueba que una determinada columna tenga exactamente 3 obstaculos
        :return: Boolean
        """
        obstaculos = ["L", "H", "V"]

        linea_1 = self.tablero[0]
        linea_2 = self.tablero[1]
        linea_3 = self.tablero[2]

        for value in range(self.tamanio - 1):
            if linea_1[value] in obstaculos:
                if not ((linea_2[value] in obstaculos) and (linea_3[value] in obstaculos)):
                    return False
            elif linea_1[value] == "0":
                if not ((linea_2[value] == "0") and (linea_3[value] == "0")):
                    return False
        return True

    def costo_local(self, robot, obs):
        """
        Se calculo el costo de vencer a un determinado obstaculo teniendo en cuenta que las reglas:
        -Piedra vence en 1 unidad de tiempo al Ladron y 2 unidades a los demas obstaculos
        -Papel vence en 1 unidad de tiempo al Homero y 2 unidades a los demas obstaculos
        -Tijera vence en 1 unidad de tiempo al Vegetal y 2 unidades a los demas obstaculos
        :param robot: String -> piedra, papel o tijera
        :param obs: String el obstaculo con el que se dispone a pelear
        :return: int costo de vencer al obstaculo
        """
        if robot == "piedra":
            if obs == "L":
                return 1
            else:
                return 2
        if robot == "papel":
            if obs == "H":
                return 1
            else:
                return 2
        else:
            if obs == "V":
                return 1
            else:
                return 2

    def without_obstacles(self, padre, next_position, robots):
        """
        Como la siguiente casilla a la del nodo(padre) esta libre, se mira cada robot el que esta libre avanza una
        casilla y el que no se descuenta 1 unidad de tiempo [pos 1]
        Avanzar una casilla tiene un costo (g) de uno, por esta razon hay que estar actualizando este valor.
        :param padre: Diccionario: nodo que a ser expandido
        :param next_position: Int: posicion + 1 del nodo que se va a expandir
        :param robots: Lista de String ['Piedra', 'Papel', 'Tijera']
        :return: void
        """
        nodo = dict(padre=padre.get("posicion_arbol"), posicion_arbol=len(self.arbol),
                    posicion_tablero=next_position,
                    g=padre.get("g"), h=(self.tamanio - next_position - 1), f="",
                    piedra=[True, -1, -1, False], papel=[True, -1, -1, False], tijera=[True, -1, -1, False])

        for value in robots:
            if padre.get(value)[0]:
                nodo[value][2] = padre.get(value)[2] + 1
                nodo["g"] = nodo.get("g") + 1
            else:
                nodo[value][1] = padre.get(value)[1] - 1
                if nodo.get(value)[1] == 0:
                    """ Si el tiempo restante es 0 significa que el robot termino de combatir, por esta razon cambia su 
                    estado a libre(True), si fue el primero en ser enviado a combatir a False"""
                    nodo[value][0] = True
                    nodo[value][1] = -1
                    nodo[value][2] = padre.get(value)[2]
                    nodo[value][3] = False
                else:
                    """ De no ser cero, los demas valores se toman del nodo expandido"""
                    nodo[value][0] = padre.get(value)[0]
                    nodo[value][2] = padre.get(value)[2]
                    nodo[value][3] = padre.get(value)[3]

        """ Se ejecuta la funcion fiend_or_enemy"""
        nodo = self.friend_or_enemy(nodo)
        """ Se calcula el valor del f, se agrega a la lista arbol y los valores de 'posicion_arbol, g y f' son 
        almacenados en la lista de prioridad"""
        nodo["f"] = nodo.get("g") + nodo.get("h")
        self.arbol.append(nodo)
        self.prioridad.append([nodo.get("posicion_arbol"), nodo.get("g"), nodo.get("f")])

    def with_obstacles(self, padre, next_position, robot_candidato, posicion_robot, obstaculos):
        """
        :param padre: Diccionario: nodo que a ser expandido
        :param next_position: Int: posicion + 1 del nodo que se va a expandir
        :param robot_candidato: 1 de los robots que posiblemente puede combatir contra los obstaculos
        :param posicion_robot: Int: la posicion del robot que se encuentra mas cerca a la meta
        :param obstaculos: List de string: Los obstaculos que se encuentra en los tres caminos en la posicion
            next_position
        :return: void
        """
        crear = True
        robots = ["piedra", "papel", "tijera"]
        robots.remove(robot_candidato)
        """ Estructura del diccionario """
        nodo = dict(padre=padre.get("posicion_arbol"), posicion_arbol=len(self.arbol),
                    posicion_tablero=0,
                    g=padre.get("g"), h=0, f=0,
                    piedra=[True, -1, -1, False], papel=[True, -1, -1, False], tijera=[True, -1, -1, False])

        if padre.get(robot_candidato)[2] == posicion_robot:
            if padre.get(robot_candidato)[2] == next_position - 1:
                """ Se valida que el robot esté libre y se encuentre en una posicion inmediatamente anterior a la del 
                obstaculo"""
                nodo[robot_candidato][0] = False
                costo_obstaculo = (self.costo_local(robot_candidato, obstaculos[0]) +
                                    self.costo_local(robot_candidato, obstaculos[1]) +
                                    self.costo_local(robot_candidato, obstaculos[2]))
                nodo[robot_candidato][1] = costo_obstaculo
                nodo[robot_candidato][2] = next_position - 1
                nodo[robot_candidato][3] = True
                nodo["posicion_tablero"] = next_position + 1
                for value in robots:
                    """ Se recorren los robots restantes"""
                    if padre.get(value)[0]:
                        if padre.get(value)[2] == next_position - 1:
                            """Si estan libres y una posicion antes de los obstaculos, saltan a 1 posicion despues de 
                            los obstaculos"""
                            nodo[value][2] = next_position + 1
                            nodo["g"] = nodo.get("g") + 1
                        else:
                            """ De lo contrario se proceden a mover 1 casilla"""
                            nodo[value][2] = padre.get(value)[2] + 1
                            nodo["g"] = nodo.get("g") + 1
                    else:
                        """ Si estan ocupados combatiendo se descuenta 1 unidad de tiempo, los otros atributos no son
                        modificados"""
                        nodo[value][0] = padre.get(value)[0]
                        nodo[value][1] = padre.get(value)[1] - 1
                        nodo[value][2] = padre.get(value)[2]
                        nodo[value][3] = padre.get(value)[3]
                nodo["g"] = nodo.get("g") + self.nodo.get(robot_candidato)[1]
            else:
                """ Si el robot mas cerca a la meta, no puede ser enviado a combatir la posicion del tablero será la 
                misma del padre por que no se puede avanzar por el tablero sin robots.
                Se procede a descontar 1 unidad de tiempo para todos los robots: 
                    - Si estan ocupados se descuenta la unidad a lo que les falta por combatir
                    - Si estan libres avanzan una casilla"""
                nodo["posicion_tablero"] = padre.get("posicion_tablero")
                robots.append(robot_candidato)
                for value in robots:
                    if padre.get(value)[0]:
                        nodo[value][2] = padre.get(value)[2] + 1
                        nodo["g"] = nodo.get("g") + 1
                    else:
                        nodo[value][0] = padre.get(value)[0]
                        nodo[value][1] = padre.get(value)[1] - 1
                        nodo[value][2] = padre.get(value)[2]
                        nodo[value][3] = padre.get(value)[3]
                robots.remove(robot_candidato)
        else:
            """Si el robot candidato no es el mas cercano a la meta, no se crea ningun nodo. Con esto se evita que 
            se creen nodos innecesarios o repetidos"""
            crear = False

        if crear:
            """ Si crear no ha sido modificado es posible crear el siguiente nodo """
            robots.append(robot_candidato)
            for value in robots:
                if nodo.get(value)[1] == 0:
                    nodo[value][0] = True
                    nodo[value][3] = False
                else:
                    pass
            nodo["h"] = self.tamanio - nodo.get("posicion_tablero") - 1
            nodo = self.friend_or_enemy(nodo)
            nodo["f"] = nodo.get("g") + nodo.get("h")
            self.arbol.append(nodo)
            self.prioridad.append([nodo.get("posicion_arbol"), nodo.get("g"), nodo.get("f")])
        else:
            pass

    def is_ayuda(self, ayuda, local):
        """
        Se determina si el robot que llega va a ayudar o por el contrario va a retardar.
        :param ayuda: Robot que llega a una determinada posicion
        :param local: Robot que se encuentra en una determianda posicion
        :return: Boolean
        """
        if ayuda == "piedra":
            if local == "tijera":
                return True
            else:
                return False
        if ayuda == "papel":
            if local == "piedra":
                return True
            else:
                return False
        if ayuda == "tijera":
            if local == "papel":
                return True
            else:
                return False

    def friend_or_enemy(self, nodo):
        """
        Se observa las posiciones de los robots del nodo, para que en tal caso de que un robot esté situado en una
        casilla en la cual otro esta combatiendo, se determine si este le ayuda o no en su progreso y el costo
        acumulado 'g' sea actualizado al igual que los atributos de estos robots.
        :param nodo: Diccionario: nodo que se expandió
        :return: nodo actualizado
        """
        robots = ["piedra", "papel", "tijera"]
        robots_update = ["", ""]
        cant_free = 0
        robots_position = []
        same_position = []

        """
        Se almacena en una lista 'robots_position' las posiciones de los 3 robots y se almacena en 'cant_free' la
        cantidad de robots libres
        """
        for value in robots:
            if nodo.get(value)[0]:
                cant_free += 1
                robots_position.append(nodo.get(value)[2])
            else:
                pass
                robots_position.append(nodo.get(value)[2])

        """Se crean un Diccionario con los valors de la lista 'robots_position' en el cual la Clave es un elemento de la 
         la lista y la respectiva llave es la cantidad de veces que se repite ese elemento"""
        counts_elem = Counter(robots_position)

        """Se identifica que robots estan en la misma posicion(columna) y sus nombres son almacenados en la lista
        same_position"""
        for indice, elem in enumerate(robots_position):
            if counts_elem[elem] > 1:
                same_position.append(robots[indice])

        if len(same_position) == 3:
            """ Si los tres estan en la misma posicion, se mira cuantos se encuentran libres"""
            if cant_free == 1:
                """ Si solo hay uno libre. 
                    -Se identifica cual es y se almacena en la posicion 0  de la lista robots_update
                    -Se identifica cual fue el primero en combatir contra los obstaculos y se almacena en  la posicion
                    1 de la lista de robots_update"""
                for value in same_position:
                    if nodo.get(value)[0]:
                        robots_update[0] = value
                    elif nodo.get(value)[3]:
                        robots_update[1] = value
                    else:
                        pass
            elif cant_free == 2:
                """ Si hay dos robots libres 
                    -Se identifica cual fue el primero en combatir contra los obstaculos y se almacena en  la posicion
                    1 de la lista de robots_update y es eliminado de la lista same_position
                    -El robot que va a prestar su 'ayuda' se toma aleatoriamente de la lista same_position y es 
                    almacenado en la lista de robots_update en la posicion 0
                    -El robot que fue eliminado es devuelto a la lista same_position, para que posteriormente sus 
                    atributos sean actualizados"""
                for value in same_position:
                    if nodo.get(value)[3]:
                        robots_update[1] = value
                        same_position.remove(value)
                        robots_update[0] = choice(same_position)
                        same_position.append(value)
                        break
                    else:
                        pass
            else:
                return nodo
        elif len(same_position) == 2:
            """ Si hay dos robots en la misma posicion"""
            if nodo.get(same_position[0])[0] != nodo.get(same_position[1])[0]:
                """ Si uno esta libre y el otro ocupado"""
                for value in same_position:
                    """Se identifica cual esta libre y se almacena en la posicion 0  de la lista robots_update.
                    El robot primero en ser enviado a combatir contra los obstaculos se almacena en  la posicion
                    1 de la lista de robots_update"""
                    if nodo.get(value)[0]:
                        robots_update[0] = value
                    else:
                        robots_update[1] = value
            else:
                """Si los dos tienen el mismo estado (libre=True o libre=False) se retorna el nodo sin modificaciones"""
                return nodo

        else:
            return nodo

        """ Se resta del costo acumulado 'g' las unidades de tiempo que restan del nodo que está combatiendo"""
        nodo["g"] = nodo.get("g") - nodo.get(robots_update[1])[1]
        if self.is_ayuda(robots_update[0], robots_update[1]):
            """ Si el robot que llega es neutral al que esta combatiendo, las unidades de tiempo se diviendo en 2"""
            time_left_updated = nodo.get(robots_update[1])[1] // 2
        else:
            """ Si el robot que llega odia al que está combatiendo, las unidades de tiempo son multiplicadas por 2"""
            time_left_updated = nodo.get(robots_update[1])[1] * 2

        """ Se actualizan los atributos de los robots """
        for value in same_position:
            nodo[value][0] = False
            nodo[value][1] = time_left_updated
            nodo[value][2] = nodo.get(value)[2]
            nodo[value][3] = nodo.get(value)[3]

        """ Si un robot ayuda al otro es posible que las unidades de tiempo que restaban se reduzcan a 0. Si ocurre
        es necesario que sean actaulizados algunos atributos de los robots """
        for value in robots:
            if nodo.get(value)[1] == 0:
                nodo[value][0] = True
                nodo[value][1] = -1
                nodo[value][2] = nodo.get(value)[2]
                nodo[value][3] = False
        nodo["g"] = nodo.get("g") + time_left_updated
        return nodo

    def insertionSort(self):
        """
        Ordena la lista de prioridad de mayor a menor en cuanto su f
        :return: void
        """
        for posicion in range(1, len(self.prioridad)):
            valorActual = self.prioridad[posicion]
            while posicion > 0 and self.prioridad[posicion - 1][2] < valorActual[2]:
                self.prioridad[posicion] = self.prioridad[posicion - 1]
                posicion = posicion - 1
                self.prioridad[posicion] = valorActual

    def criterio_costo(self):
        """
        En ocasiones el f de los nodos puede ser el mismo, por esta razon se buscan el nodo que tenga con costo
        acumulado g menor al resto y que se encuentre mas a la izquierda de la rama
        :return: int: posicion del nodo en el arbol
        """
        self.insertionSort()
        """ Permite recorrer la lista prioridad de atras hacia adelante"""
        indice = -1
        tupla_costo = [-1, -1, -1]
        for value in range(len(self.prioridad)):
            """ La lista prioridad es recorrida de atras hacia adelante"""
            if tupla_costo[2] == -1:
                """ Si es el ultimo elemento se actuliza la lista auxiliar tupla_costo"""
                for i in range(3):
                    tupla_costo[i] = self.prioridad[indice][i]
            else:
                """ Se compara el 'f' del elemento actual con el de la lista auxiliar"""
                if self.prioridad[indice][2] == tupla_costo[2]:
                    """ Si tienen igual f, se toma el que tiene menor costo acumulado 'g' y la lista auxiliar es
                    actualizada"""
                    if self.prioridad[indice][1] <= tupla_costo[1]:
                        for i in range(3):
                            tupla_costo[i] = self.prioridad[indice][i]
                    else:
                        pass
                else:
                    """ El ciclo termina cuando se acabaen los elementos de la lista prioridad o los elementos a
                    comparar tengan un f distinto"""
                    break
            indice += -1
        """ El elememento candidato es eliminado de la lista prioridad"""
        self.prioridad.remove(tupla_costo)
        return tupla_costo[0]

    def recorrer_camino_optimo(self, nodo):
        """
        En la lista camino_optimo es almacenada la tupla con los robots (piedra, papel, tijera) de cada nodo.
        Se realiza un recursion pero esta vez se envia como parametro el nodo ubicado en la lista arbol
         en la posicion (padre) del nodo actual.
        La recursion finaliza cuando el padre de alguno nodo sea 'null'
        :param nodo: Diccionario: nodo que llegó a la meta
        :return: void
        """
        if nodo.get("padre") == "null":
            pass
        else:
            tupla_aux = (nodo.get("piedra"), nodo.get("papel"), nodo.get("tijera"))
            self.camino_optimo.insert(0, tupla_aux)
            nodo_aux = self.arbol[nodo.get("padre")]
            self.recorrer_camino_optimo(nodo_aux)

    def number_one(self, nodo):
        """
        Se compara la posicion de los 3 robots del nodo
        :param nodo: Diccionarion: nodo que va a ser expandido
        :return: La posicion del robot mas cerca a la meta
        """
        posicion_piedra = nodo.get("piedra")[2]
        posicion_papel = nodo.get("papel")[2]
        posicion_tijera = nodo.get("tijera")[2]
        return max(posicion_piedra, posicion_papel, posicion_tijera)

    def algoritmo_a(self):
        """
        Se expanden el nodo que esté en la ultima posición de la lista prioridad.
        Los diferentes casos para expandir un nodo son los siguientes:
            -Que  tenga heuristica == 1, lo que indica que está aun paso de la meta
            -Que en la siguiente posicion al tablero (posicion_tablero+1) se encuentren obstaculos
            -Que en la siguiente posicion al tablero (posicion_tablero+1) se encuentren casillas en blanco
        :return: void
        """
        stop = False
        obstaculos = ["L", "H", "V"]
        """ Se extraen las filas del tablero, que representan los caminos a recorrer por los robots"""
        camino_uno = self.tablero[0]
        camino_dos = self.tablero[1]
        camino_tres = self.tablero[2]
        while not stop:
            """ Se extrae del arbol la posicion que indica la ultima tupla de la lista prioridad"""
            mejor_opcion = self.arbol[self.criterio_costo()]
            """ next_position = la posicion en la que esta el nodo a expandir + 1. Con esto se  mira en los caminos si 
            en esa posicion (next_posicion) hay casillas en blanco u obstaculos."""
            next_position = mejor_opcion.get("posicion_tablero") + 1

            if mejor_opcion.get("h") == 1:
                """ Se verifica si algun robor está libre y a un paso de la meta, de ser asi, se detiene el ciclo
                while. De lo contrario se ejecuta el la funcion 'without_obstacales'"""
                if ((mejor_opcion.get("piedra")[2] == next_position - 1 and mejor_opcion.get("piedra")[0]) or
                        (mejor_opcion.get("papel")[2] == next_position - 1 and mejor_opcion.get("papel")[0]) or
                        (mejor_opcion.get("tijera")[2] == next_position - 1 and mejor_opcion.get("tijera")[0])):
                    stop = True
                else:
                    self.without_obstacles(mejor_opcion, next_position - 1, ["piedra", "papel", "tijera"])
            else:
                if camino_uno[next_position] in obstaculos:
                    """ Si en la siguiente posicion hay obstaculos, se ejecuta la funcion 'with_obstacles' 3 veces
                    que representa la posibilidad de enviar a combatir a piedra, papel o a tijera"""
                    posicion_robot = self.number_one(mejor_opcion)
                    self.with_obstacles(mejor_opcion, next_position, "piedra", posicion_robot,
                                        [camino_uno[next_position], camino_dos[next_position],
                                         camino_tres[next_position]])
                    self.with_obstacles(mejor_opcion, next_position, "papel", posicion_robot,
                                        [camino_uno[next_position], camino_dos[next_position],
                                         camino_tres[next_position]])
                    self.with_obstacles(mejor_opcion, next_position, "tijera", posicion_robot,
                                        [camino_uno[next_position], camino_dos[next_position],
                                         camino_tres[next_position]])
                else:
                    """ Si en la siguiente posicion hay casillas vacias se ejecuta la funcion 'without_obstacles'"""
                    self.without_obstacles(mejor_opcion, next_position, ["piedra", "papel", "tijera"])
        """ Cuando termina el ciclo, es por que el ultimo nodo expandido llegó a la meta por esta razon se ejecuta la 
        'funcion recorrer_camino_optimo'"""
        self.recorrer_camino_optimo(mejor_opcion)
        for value in self.camino_optimo:
            print(value)
        print(len(self.arbol))