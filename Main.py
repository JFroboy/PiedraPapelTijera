import sys
import GUI
from PyQt5 import uic, QtWidgets

qtCreatorFile = "GUI.ui"
Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)

class MyApp(QtWidgets.QMainWindow, GUI.Ui_MainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        GUI.Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.button_cargar.clicked.connect(self.cargar_tablero)
        self.button_go.clicked.connect(self.go)




if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())
    