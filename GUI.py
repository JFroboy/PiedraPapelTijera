# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'GUI.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!
import sys
import time

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtGui import QPixmap, QIcon
from tkinter import *

from PyQt5.QtWidgets import QFileDialog

from index import Read

class Ui_MainWindow(object):
    o = Read()
    pos_enemig = [[], [], []]
    hay_enemig = False
    ins1 = 0
    ins2 = 0
    ins3 = 0

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(690, 580)
        MainWindow.setMinimumSize(QtCore.QSize(677, 574))
        MainWindow.setAutoFillBackground(False)
        MainWindow.setStyleSheet("background-color: rgb(168, 168, 168);")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.frame_label = QtWidgets.QFrame(self.centralwidget)
        self.frame_label.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_label.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_label.setObjectName("frame_label")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame_label)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.label = QtWidgets.QLabel(self.frame_label)
        self.label.setMinimumSize(QtCore.QSize(0, 29))
        self.label.setObjectName("label")
        self.gridLayout_2.addWidget(self.label, 0, 0, 1, 1)
        self.label_error = QtWidgets.QLabel(self.frame_label)
        self.label_error.setText("")
        self.label_error.setAlignment(QtCore.Qt.AlignCenter)
        self.label_error.setObjectName("label_error")
        self.gridLayout_2.addWidget(self.label_error, 1, 0, 1, 1)
        self.verticalLayout.addWidget(self.frame_label)
        self.frame_button = QtWidgets.QFrame(self.centralwidget)
        self.frame_button.setMinimumSize(QtCore.QSize(0, 65))
        self.frame_button.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_button.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_button.setObjectName("frame_button")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.frame_button)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.button_cargar = QtWidgets.QPushButton(self.frame_button)
        self.button_cargar.setStyleSheet("background-color: rgb(0, 85, 255);\n"
                                         "border-color: rgb(0, 0, 0);\n"
                                         "font: 75 12pt \"News706 BT\";")
        self.button_cargar.setObjectName("button_cargar")
        self.horizontalLayout.addWidget(self.button_cargar)
        self.verticalLayout.addWidget(self.frame_button)

        self.frame_tabla = QtWidgets.QFrame(self.centralwidget)
        self.frame_tabla.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_tabla.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_tabla.setObjectName("frame_tabla")
        self.gridLayout = QtWidgets.QGridLayout(self.frame_tabla)
        self.gridLayout.setObjectName("gridLayout")
        self.table_game = QtWidgets.QTableWidget(self.frame_tabla)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.table_game.sizePolicy().hasHeightForWidth())
        self.table_game.setObjectName("table_game")
        self.table_game.setSizePolicy(sizePolicy)
        self.table_game.setMinimumSize(QtCore.QSize(256, 301))
        self.table_game.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.table_game.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.table_game.setIconSize(QtCore.QSize(80, 80))
        self.table_game.horizontalHeader().setDefaultSectionSize(80)
        self.table_game.verticalHeader().setDefaultSectionSize(80)

        self.gridLayout.addWidget(self.table_game, 0, 0, 1, 1)
        self.verticalLayout.addWidget(self.frame_tabla)

        self.frame_go = QtWidgets.QFrame(self.centralwidget)
        self.frame_go.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_go.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_go.setObjectName("frame_go")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.frame_go)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.button_go = QtWidgets.QPushButton(self.frame_go)
        self.button_go.setStyleSheet("background-color: rgb(0, 85, 255);\n"
                                     "font: 20pt \"Ravie\";")
        self.button_go.setObjectName("button_go")
        self.verticalLayout_4.addWidget(self.button_go)
        self.verticalLayout.addWidget(self.frame_go)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))

        self.label.setText(_translate("MainWindow",
                                      "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt; font-weight:600;\">Maratón de Robots</span></p></body></html>"))
        self.button_cargar.setText(_translate("MainWindow", "Cargar Tablero"))
        self.button_go.setToolTip(_translate("MainWindow",
                                             "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt; font-weight:600;\">GO!</span></p></body></html>"))
        self.button_go.setText(_translate("MainWindow", "GO!"))

    def cargar_tablero(self):
        """
        Cuando se presiona el boton cargar se despliega una ventana para que se pueda seleccionar el archivo de texto
        Si se cumplen las restrincciones se procede a inicializar los valores en la tabla (numeros e imagenes)
        Si estas no se cumplen se muestra un label con indicando el error
        :return: void
        """
        self.label_error.setText("")
        file_name = ((QFileDialog.getOpenFileNames(self, "Open File","C://"))[0])[0]

        if self.o.len_read_file(file_name):
            self.o.read_file(file_name)
            if self.o.validations_tablero():
                self.table_game.setRowCount(4)
                self.table_game.setColumnCount(self.o.tamanio + 1)
                self.table_game.setRowHeight(0, 5)

                """For para preparar las filas"""
                for value in range(4):
                    item = QtWidgets.QTableWidgetItem()
                    self.table_game.setVerticalHeaderItem(value, item)

                """For para preparar las columnas"""
                for value in range(self.o.tamanio + 1):
                    item = QtWidgets.QTableWidgetItem()
                    self.table_game.setHorizontalHeaderItem(value, item)

                """For para preparar el resto de celdas"""
                for row in range(0, 4):
                    for col in range(0, self.o.tamanio + 1):
                        item = QtWidgets.QTableWidgetItem()
                        self.table_game.setItem(row, col, item)

                _translate = QtCore.QCoreApplication.translate

                """ Se escriben los respectivos valores en los encabezados de las columnas """
                item = self.table_game.horizontalHeaderItem(0)
                item.setText(_translate("MainWindow", "Salida"))
                item = self.table_game.horizontalHeaderItem(self.o.tamanio)
                item.setText(_translate("MainWindow", "Llegada"))

                for value in range(1, self.o.tamanio):
                    item = self.table_game.horizontalHeaderItem(value)
                    s = "{}".format(value)
                    item.setText(_translate("MainWindow", s))

                __sortingEnabled = self.table_game.isSortingEnabled()
                self.table_game.setSortingEnabled(False)
                """ ====================================================================== """

                """ Se cargan las imagenes de la meta y de los tres robots"""
                for row in range(1, 4):
                    pixmap = QPixmap('resources/images/meta.png')
                    item = self.table_game.item(row, self.o.tamanio)
                    item.setIcon(QIcon(pixmap))

                pixmap = QPixmap('resources/images/piedra.png')
                item = self.table_game.item(1, 0)
                item.setIcon(QIcon(pixmap))

                pixmap = QPixmap('resources/images/papel.jpg')
                item = self.table_game.item(2, 0)
                item.setIcon(QIcon(pixmap))

                pixmap = QPixmap('resources/images/tijera.jpg')
                item = self.table_game.item(3, 0)
                item.setIcon(QIcon(pixmap))
                """ ======================================================="""

                """ Se lee el tablero obtenido al leer el documento de texto, para ubicar los obstaculos en su 
                respectiva posicion"""
                for row in range(0, 3):
                    camino = self.o.tablero[row]
                    i = 0
                    for value in camino:
                        if value == "V":
                            pixmap = QPixmap('resources/images/vegetal.jpg')
                            item = self.table_game.item(row + 1, i + 1)
                            item.setIcon(QIcon(pixmap))
                            self.pos_enemig[0].append((row + 1, i + 1))
                            self.hay_enemig=True
                        elif value == "L":
                            pixmap = QPixmap('resources/images/ladron.png')
                            item = self.table_game.item(row + 1, i + 1)
                            item.setIcon(QIcon(pixmap))
                            self.pos_enemig[1].append((row + 1, i + 1))
                            self.hay_enemig=True
                        elif value == "H":
                            pixmap = QPixmap('resources/images/homero.jpg')
                            item = self.table_game.item(row + 1, i + 1)
                            item.setIcon(QIcon(pixmap))
                            self.pos_enemig[2].append((row + 1, i + 1))
                            self.hay_enemig=True

                        i += 1
                self.table_game.setSortingEnabled(__sortingEnabled)
            else:
                self.label_error.setText("<html><head/><body><p align=\"center\"><span style=\" "
                                         "font-size:8pt; color:#ff0000; font-weight:600;\""
                                         ">Por favor revise la guia ubicada en resources/input/ README"
                                         "</span></p></body></html>")
        else:
            self.label_error.setText("<html><head/><body><p align=\"center\"><span style=\" "
                                     "font-size:8pt; color:#ff0000; font-weight:600;\""
                                     ">Por favor revise la guia del archivo resources/input/ README"
                                     "</span></p></body></html>")

    def prueba(self, roca, papel, tijera):
        pixmap = QPixmap('resources/images/piedra.png')
        item = self.table_game.item(1, roca)
        item.setIcon(QIcon(pixmap))
        if not self.hay_enemig:
            pixmap = QPixmap('resources/images/piedras.png')
            item = self.table_game.item(1, roca-1)
            item.setIcon(QIcon(pixmap))
        else:
            pass
        pixmap = QPixmap('resources/images/papel.jpg')
        item = self.table_game.item(2, papel)
        item.setIcon(QIcon(pixmap))
        if not self.hay_enemig:
            pixmap = QPixmap('resources/images/papels.jpg')
            item = self.table_game.item(2, papel-1)
            item.setIcon(QIcon(pixmap))
        else:
            pass
        
        pixmap = QPixmap('resources/images/tijera.jpg')
        item = self.table_game.item(3, tijera)
        item.setIcon(QIcon(pixmap))
        if not self.hay_enemig:
            pixmap = QPixmap('resources/images/tijeras.jpg')
            item = self.table_game.item(3, tijera-1)
            item.setIcon(QIcon(pixmap))
    
    def go(self):
        raiz = Tk()
        self.o.algoritmo_a()
        pixmap = QPixmap('resources/images/pelea.jpg')
        pixmap2 = QPixmap('resources/images/peleas.jpg')
        self.borrar(0, 0, 0)

        for val in range(len(self.o.camino_optimo)):
            self.prueba(self.o.camino_optimo[val][0][2]+1, self.o.camino_optimo[val][1][2]+1, self.o.camino_optimo[val][2][2]+1)
            self.borrar(self.o.camino_optimo[val-1][0][2]+1, self.o.camino_optimo[val-1][1][2]+1, self.o.camino_optimo[val-1][2][2]+1)
            
            if self.o.camino_optimo[val][0][3]:
                self.ins1 = self.o.camino_optimo[val][0][2]
                item = self.table_game.item(1, self.o.camino_optimo[val][0][2]+2)
                item.setIcon(QIcon(pixmap))
                item = self.table_game.item(2, self.o.camino_optimo[val][0][2]+2)
                item.setIcon(QIcon(pixmap))
                item = self.table_game.item(3, self.o.camino_optimo[val][0][2]+2)
                item.setIcon(QIcon(pixmap))
            if (self.ins1+2) == self.o.camino_optimo[val][0][2]:
                item = self.table_game.item(1, self.o.camino_optimo[val-1][0][2]+1)
                item.setIcon(QIcon(pixmap2))
                item = self.table_game.item(2, self.o.camino_optimo[val-1][0][2]+1)
                item.setIcon(QIcon(pixmap2))
                item = self.table_game.item(3, self.o.camino_optimo[val-1][0][2]+1)
                item.setIcon(QIcon(pixmap2))
                self.ins1 = 0
            if self.o.camino_optimo[val][1][3]:
                self.ins2 = self.o.camino_optimo[val][1][2]
                item = self.table_game.item(1, self.o.camino_optimo[val][1][2]+2)
                item.setIcon(QIcon(pixmap))
                item = self.table_game.item(2, self.o.camino_optimo[val][1][2]+2)
                item.setIcon(QIcon(pixmap))
                item = self.table_game.item(3, self.o.camino_optimo[val][1][2]+2)
                item.setIcon(QIcon(pixmap))
            if (self.ins2+2) == self.o.camino_optimo[val][1][2]:
                item = self.table_game.item(1, self.o.camino_optimo[val-1][1][2]+1)
                item.setIcon(QIcon(pixmap2))
                item = self.table_game.item(2, self.o.camino_optimo[val-1][1][2]+1)
                item.setIcon(QIcon(pixmap2))
                item = self.table_game.item(3, self.o.camino_optimo[val-1][1][2]+1)
                item.setIcon(QIcon(pixmap2))
                self.ins2 = 0
            if self.o.camino_optimo[val][2][3]:
                self.ins3=self.o.camino_optimo[val][2][2]
                item = self.table_game.item(1, self.o.camino_optimo[val][2][2]+2)
                item.setIcon(QIcon(pixmap))
                item = self.table_game.item(2, self.o.camino_optimo[val][2][2]+2)
                item.setIcon(QIcon(pixmap))
                item = self.table_game.item(3, self.o.camino_optimo[val][2][2]+2)
                item.setIcon(QIcon(pixmap))
            if (self.ins3+2) == self.o.camino_optimo[val][2][2]:
                item = self.table_game.item(1, self.o.camino_optimo[val-1][2][2]+1)
                item.setIcon(QIcon(pixmap2))
                item = self.table_game.item(2, self.o.camino_optimo[val-1][2][2]+1)
                item.setIcon(QIcon(pixmap2))
                item = self.table_game.item(3, self.o.camino_optimo[val-1][2][2]+1)
                item.setIcon(QIcon(pixmap2))
                self.ins3 = 0
            time.sleep(1.5)
            raiz.update()
            raiz.iconify()

    def borrar(self, roca, papel, tijera):
        pixmap = QPixmap('resources/images/piedrs.png')
        item = self.table_game.item(1, roca)
        item.setIcon(QIcon(pixmap))

        pixmap = QPixmap('resources/images/papels.jpg')
        item = self.table_game.item(2, papel)
        item.setIcon(QIcon(pixmap))

        pixmap = QPixmap('resources/images/tijeras.jpg')
        item = self.table_game.item(3, tijera)
        item.setIcon(QIcon(pixmap))
